package app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = {"app", "service", "repositories", "convert", "entities", "dto"})
@EntityScan(basePackages = {"entities"})
@EnableJpaRepositories(basePackages = {"repositories"})
@Import(SwaggerConfig.class)
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
        // Swagger at : http://localhost:8080/swagger-ui
    }
}
