package app.controller;

import dto.PlaceDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.place.IPlaceService;

import java.util.List;

@RestController
@RequestMapping("/lieux")
@Tag(name = "Lieux")
@CrossOrigin(origins = "http://localhost:5173")
public class PlaceController {
    private final IPlaceService placeService;

    public PlaceController(@Qualifier("placeServiceImpl") IPlaceService placeService) {
        this.placeService = placeService;
    }

    @GetMapping("/")
    @Operation(summary = "Récupérer tous les lieux.")
    public ResponseEntity<List<PlaceDTO>> getPlaces() {
        return ResponseEntity.ok(placeService.getAllPlaces());
    }

    @GetMapping("/{id}")
    @Operation(summary = "Récupérer une salle par identifiant.")
    public ResponseEntity<PlaceDTO> getPlaceByID(@PathVariable int id) {
        return ResponseEntity.ok(placeService.getPlaceByID(id));
    }

    @GetMapping("/salle/{room}")
    @Operation(summary = "Récupérer une liste de salle par nom.")
    public ResponseEntity<List<PlaceDTO>> getPlaceByRoom(@PathVariable String room) {
        return ResponseEntity.ok(placeService.findFirstByRoom(room));
    }

    @GetMapping("/batiment/{building}")
    @Operation(summary = "Récupérer une liste de salle par batiment.")
    public ResponseEntity<List<PlaceDTO>> getPlaceByBuilding(@PathVariable String building) {
        return ResponseEntity.ok(placeService.findAllByBuilding(building));
    }

    @GetMapping("/type_salle/{roomType}")
    @Operation(summary = "Récupérer une liste de salle par type de salle.")
    public ResponseEntity<List<PlaceDTO>> getPlaceByRoomType(@PathVariable String roomType) {
        return ResponseEntity.ok(placeService.findAllByRoomType(roomType));
    }

    @GetMapping("/id_materiel/{deviceID}")
    @Operation(summary = "Récupérer une liste de salle contenant un matériel par identifiant.")
    public ResponseEntity<List<PlaceDTO>> getPlaceByDeviceID(@PathVariable int deviceID) {
        return ResponseEntity.ok(placeService.findAllByDeviceID(deviceID));
    }

    @PostMapping("/")
    @Operation(summary = "Ajouter une salle.")
    public ResponseEntity<Void> addPlace(@RequestBody PlaceDTO place) {
        placeService.addPlace(place);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Supprimer une salle.")
    public ResponseEntity<Void> removePlace(@PathVariable int id) {
        placeService.removePlace(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/")
    @Operation(summary = "Modifier une salle.")
    public ResponseEntity<Void> modifyPlace(@RequestBody PlaceDTO place) {
        placeService.modifyPlace(place);
        return ResponseEntity.ok().build();
    }
}
