package app.controller;

import dto.TimeSlotDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.timeslot.ITimeSlotService;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/creneaux")
@Tag(name = "Créneaux")
@CrossOrigin(origins = "http://localhost:5173")
public class TimeSlotController {
    private final ITimeSlotService timeSlotService;

    public TimeSlotController(@Qualifier("timeSlotServiceImpl") ITimeSlotService timeSlotService) {
        this.timeSlotService = timeSlotService;
    }

    @GetMapping("/")
    @Operation(summary = "Récupérer tous les créneaux.")
    public ResponseEntity<List<TimeSlotDTO>> getTimeSlots() {
        return ResponseEntity.ok(timeSlotService.getAllTimeSlots());
    }

    @GetMapping("/{id}")
    @Operation(summary = "Récupérer le créneau correspondant avec un identifiant.")
    public ResponseEntity<TimeSlotDTO> getTimeSlotById(@PathVariable int id) {
        return ResponseEntity.ok(timeSlotService.getTimeSlotByID(id));
    }

    @GetMapping("/lieu/{id}")
    @Operation(summary = "Récupérer les créneaux par identifiant de lieu.")
    public ResponseEntity<List<TimeSlotDTO>> getTimeSlotsByPlaceID(@PathVariable int id) {
        return ResponseEntity.ok(timeSlotService.getTimeSlotsByPlaceID(id));
    }

    @GetMapping("/employe/{id}")
    @Operation(summary = "Récupérer les créneaux concernant un employé par identifiant.")
    public ResponseEntity<List<TimeSlotDTO>> getTimeSlotsByEmployeeID(@PathVariable int id) {
        return ResponseEntity.ok(timeSlotService.getTimeSlotsByEmployeeID(id));
    }

    @GetMapping("/promotion/{id}")
    @Operation(summary = "Récupérer les créneaux concernant une promotion par identifiant.")
    public ResponseEntity<List<TimeSlotDTO>> getTimeSlotsByPromotionID(@PathVariable int id) {
        return ResponseEntity.ok(timeSlotService.getTimeSlotsByPromotionID(id));
    }

    @GetMapping("/date/{date}")
    @Operation(summary = "Récupérer les créneaux après une date.")
    public ResponseEntity<List<TimeSlotDTO>> getTimeSlotsByStartDate(@PathVariable LocalDateTime date) {
        return ResponseEntity.ok(timeSlotService.getTimeSlotsByStartDate(date));
    }

    @PostMapping("/")
    @Operation(summary = "Ajouter un créneau.")
    public ResponseEntity<Void> addTimeSlot(@RequestBody TimeSlotDTO timeSlot) {
        timeSlotService.addTimeSlot(timeSlot);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/")
    @Operation(summary = "Modifier un créneau.")
    public ResponseEntity<Void> updateTimeSlot(@RequestBody TimeSlotDTO timeSlot) {
        timeSlotService.updateTimeSlot(timeSlot);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Supprimer un créneau.")
    public ResponseEntity<Void> deleteTimeSlot(@PathVariable int id) {
        timeSlotService.deleteTimeSlot(id);
        return ResponseEntity.ok().build();
    }
}
