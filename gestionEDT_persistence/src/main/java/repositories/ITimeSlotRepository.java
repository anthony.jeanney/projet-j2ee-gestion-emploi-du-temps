package repositories;

import entities.TimeSlotEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ITimeSlotRepository extends JpaRepository<TimeSlotEntity, Integer> {
    List<TimeSlotEntity> findByStartDateOrderByStartDateAsc (LocalDateTime startDate);
    List<TimeSlotEntity> findAllByPlaceID (int placeID);
    List<TimeSlotEntity> findAllByEmployeeIDsContaining (int employeeID);
    List<TimeSlotEntity> findAllByPromotionIDsContaining (int promotionID);
}
