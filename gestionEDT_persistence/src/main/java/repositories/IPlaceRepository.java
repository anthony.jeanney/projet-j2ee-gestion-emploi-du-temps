package repositories;

import entities.PlaceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IPlaceRepository extends JpaRepository<PlaceEntity, Integer> {
    List<PlaceEntity> findFirstByRoom (String room);
    List<PlaceEntity> findAllByRoomType (String roomType);
    List<PlaceEntity> findAllByBuilding (String building);
    List<PlaceEntity> findAllByDeviceIDsContaining (int deviceID);
}
