package entities;

import jakarta.persistence.*;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "creneau")
public class TimeSlotEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_creneau")
    private int timeslotID;

    @Column(name = "id_lieu")
    private int placeID;

    @ElementCollection
    @CollectionTable(name = "creneau_employe", joinColumns = @JoinColumn(name = "id_creneau"))
    @Column(name = "ids_employee")
    private List<Integer> employeeIDs;

    @ElementCollection
    @CollectionTable(name = "creneau_promotion", joinColumns = @JoinColumn(name = "id_creneau"))
    @Column(name = "ids_promotion")
    private List<Integer> promotionIDs;

    @Column(name = "id_matiere")
    private int materialID;

    @Column(name = "date_debut")
    private LocalDateTime startDate;

    @Column(name = "date_fin")
    private LocalDateTime endDate;

    public int getTimeslotID() {
        return timeslotID;
    }

    public void setTimeslotID(int id_creneau) {
        this.timeslotID = id_creneau;
    }

    public int getPlaceID() {
        return placeID;
    }

    public void setPlaceID(int id_lieu) {
        this.placeID = id_lieu;
    }

    public List<Integer> getEmployeeIDs() {
        return employeeIDs;
    }

    public void setEmployeeIDs(List<Integer> employeeIDs) {
        this.employeeIDs = employeeIDs;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime dateDebut) {
        this.startDate = dateDebut;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime dateFin) {
        this.endDate = dateFin;
    }

    public List<Integer> getPromotionIDs() {
        return promotionIDs;
    }

    public void setPromotionIDs(List<Integer> promotionIDs) {
        this.promotionIDs = promotionIDs;
    }

    public int getMaterialID() {
        return materialID;
    }

    public void setMaterialID(int materialID) {
        this.materialID = materialID;
    }
}
