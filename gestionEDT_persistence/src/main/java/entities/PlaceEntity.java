package entities;

import jakarta.persistence.*;

import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "lieu")
public class PlaceEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_lieu")
    private int placeID;

    @Column(name = "salle")
    private String room;

    @Column(name = "type_salle")
    private String roomType;

    @Column(name = "batiment")
    private String building;

    @ElementCollection
    @CollectionTable(name = "lieu_materiel", joinColumns = @JoinColumn(name = "id_lieu"))
    @Column(name = "ids_materiels")
    private List<Integer> deviceIDs;

    public int getPlaceID() {
        return placeID;
    }

    public void setPlaceID(int id_lieu) {
        this.placeID = id_lieu;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String salle) {
        this.room = salle;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String type_solution) {
        this.roomType = type_solution;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String batiment) {
        this.building = batiment;
    }

    public List<Integer> getDeviceIDs() {
        return deviceIDs;
    }

    public void setDeviceIDs(List<Integer> id_materiels) {
        this.deviceIDs = id_materiels;
    }
}
