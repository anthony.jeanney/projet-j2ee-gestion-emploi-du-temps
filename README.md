# Projet J2EE Gestion Emploi du Temps

Projet J2EE dans le cadre de la formation de Master 2 WeDSci de l'Université du Littoral Côte d'Opale.

Le but de ce projet est de réaliser un logiciel de gestion de l'université.

Différents modules sont à réaliser :

- Gestion du personnel d'un point de vue RH
- Gestion du personnel d'un point de vue comptabilité
- Gestion des promos
- Gestion des matériels
- Gestion de l'emploi du temps
- Gestion de la bibliothèque

Nous avons choisi de réaliser le module de gestion de l'emploi du temps.

## Technologies utilisées

- Base de données : MySQL
- Langage Applicatif : Java
- Framework Back : SpringBoot
- Framework Front : VueJS
- Bibliothèque CSS : Bootstrap

[Lien vers Trello](https://trello.com/b/EBDUXUOO/gestion-emploi-du-temps)

## Installation

### Base de données

Une base de données MySQL commune a été mise en place par nous même. Elle a été mise à disposition pour les autres groupes. Celle-ci a été déployée sur un serveur distant oracle cloud personnel :

[mysql://oracle.anthony-jeanney.fr:3306/j2ee](mysql://oracle.anthony-jeanney.fr:3306/j2ee)

### Lancement du backend

- Cloner le projet
- Dans le dossier du projet, compiler le projet avec Maven : `mvn clean install`
- Un jar utilisable est généré dans le dossier gestionEDT_presentation : `java -jar gestionEDT_presentation-VERSION.jar`
- Le backend est alors accessible sur le port 8080

### Documentation

La documentation du projet est disponible sur un swagger : [http://localhost:8080/swagger-ui](http://localhost:8080/swagger-ui)

### Déploiement

Un API Gateway a été mis en place par notre groupe pour permettre de déployer le projet sur un serveur distant et mettre en commun les différents modules. Ce dernier utilise docker et nginx pour fonctionner. Les configurations sont disponibles dans le dossier `gateway`. Le tout est installé sur un serveur distant Oracle Cloud personnel.

[https://oracle.anthony-jeanney.fr/gestionEDT/api](https://oracle.anthony-jeanney.fr/gestionEDT/api)

### Frontend

Le frontend est disponible sur le dépôt suivant : [https://gitlab.com/anthony.jeanney/projet-j2ee-gestion-emploi-du-temps-front](https://gitlab.com/anthony.jeanney/projet-j2ee-gestion-emploi-du-temps-front)