package service.place;

import org.springframework.stereotype.Service;
import repositories.IPlaceRepository;
import convert.PlaceConvert;
import dto.PlaceDTO;

import java.util.List;

@Service
public class PlaceServiceImpl implements IPlaceService {
    private final IPlaceRepository placeRepository;
    private final PlaceConvert placeConvert;

    public PlaceServiceImpl(IPlaceRepository placeRepository) {
        this.placeRepository = placeRepository;
        this.placeConvert= PlaceConvert.getInstance();
    }

    @Override
    public void addPlace(PlaceDTO place) {
        placeRepository.save(
                placeConvert.convertDTOToEntity(place)
        );
    }

    @Override
    public void removePlace(int id) {
        placeRepository.deleteById(id);
    }

    @Override
    public void modifyPlace(PlaceDTO place) {
        placeRepository.save(
                placeConvert.convertDTOToEntity(place)
        );
    }

    @Override
    public PlaceDTO getPlaceByID(int id) {
        return placeConvert.convertEntityToDTO(
                placeRepository.findById(id).orElse(null)
        );
    }

    @Override
    public List<PlaceDTO> getAllPlaces() {
        return placeConvert.convertEntityToDTO(placeRepository.findAll());
    }

    @Override
    public List<PlaceDTO> findFirstByRoom(String room) {
        return placeConvert.convertEntityToDTO(
                placeRepository.findFirstByRoom(room)
        );
    }

    @Override
    public List<PlaceDTO> findAllByBuilding(String building) {
        return placeConvert.convertEntityToDTO(
                placeRepository.findAllByBuilding(building)
        );
    }

    @Override
    public List<PlaceDTO> findAllByRoomType(String roomType) {
        return placeConvert.convertEntityToDTO(
                placeRepository.findAllByRoomType(roomType)
        );
    }

    @Override
    public List<PlaceDTO> findAllByDeviceID(int deviceID) {
        return placeConvert.convertEntityToDTO(
                placeRepository.findAllByDeviceIDsContaining(deviceID)
        );
    }
}
