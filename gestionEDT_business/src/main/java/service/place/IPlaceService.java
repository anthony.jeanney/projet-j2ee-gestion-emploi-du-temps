package service.place;

import dto.PlaceDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IPlaceService {
    void addPlace(PlaceDTO place);
    void removePlace(int id);
    void modifyPlace(PlaceDTO place);

    PlaceDTO getPlaceByID(int id);
    List<PlaceDTO> getAllPlaces();
    List<PlaceDTO> findFirstByRoom(String room);
    List<PlaceDTO> findAllByBuilding(String building);
    List<PlaceDTO> findAllByRoomType(String roomType);
    List<PlaceDTO> findAllByDeviceID(int deviceID);
}
