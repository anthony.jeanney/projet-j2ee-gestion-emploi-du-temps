package service.timeslot;

import org.springframework.stereotype.Service;
import repositories.ITimeSlotRepository;
import convert.TimeSlotConvert;
import dto.TimeSlotDTO;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class TimeSlotServiceImpl implements ITimeSlotService {
    private final ITimeSlotRepository timeSlotRepository;
    private final TimeSlotConvert timeSlotConvert;

    public TimeSlotServiceImpl(ITimeSlotRepository timeSlotRepository) {
        this.timeSlotRepository = timeSlotRepository;
        this.timeSlotConvert = TimeSlotConvert.getInstance();
    }

    @Override
    public TimeSlotDTO getTimeSlotByID(int id) {
        return timeSlotConvert.convertEntityToDTO(timeSlotRepository.findById(id).orElse(null));
    }

    @Override
    public void addTimeSlot(TimeSlotDTO timeSlotDTO) {
        timeSlotRepository.save(timeSlotConvert.convertDTOToEntity(timeSlotDTO));
    }

    @Override
    public void updateTimeSlot(TimeSlotDTO timeSlotDTO) {
        timeSlotRepository.save(timeSlotConvert.convertDTOToEntity(timeSlotDTO));
    }

    @Override
    public void deleteTimeSlot(int id) {
        timeSlotRepository.deleteById(id);
    }

    @Override
    public List<TimeSlotDTO> getAllTimeSlots() {
        return timeSlotConvert.convertEntityToDTO(timeSlotRepository.findAll());
    }

    @Override
    public List<TimeSlotDTO> getTimeSlotsByPlaceID(int placeID) {
        return timeSlotConvert.convertEntityToDTO(timeSlotRepository.findAllByPlaceID(placeID));
    }

    @Override
    public List<TimeSlotDTO> getTimeSlotsByEmployeeID(int employeeID) {
        return timeSlotConvert.convertEntityToDTO(timeSlotRepository.findAllByEmployeeIDsContaining(employeeID));
    }

    @Override
    public List<TimeSlotDTO> getTimeSlotsByPromotionID(int promotionID) {
        return timeSlotConvert.convertEntityToDTO(timeSlotRepository.findAllByPromotionIDsContaining(promotionID));
    }

    @Override
    public List<TimeSlotDTO> getTimeSlotsByStartDate(LocalDateTime startDate) {
        return timeSlotConvert.convertEntityToDTO(timeSlotRepository.findByStartDateOrderByStartDateAsc(startDate));
    }

    @Override
    public TimeSlotDTO getLatestTimeSlot() {
        return timeSlotConvert.convertEntityToDTO(timeSlotRepository.findAll().get(timeSlotRepository.findAll().size() - 1));
    }
}
