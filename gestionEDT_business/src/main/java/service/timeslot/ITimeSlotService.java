package service.timeslot;

import dto.TimeSlotDTO;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public interface ITimeSlotService {
    TimeSlotDTO getTimeSlotByID(int id);
    void addTimeSlot(TimeSlotDTO timeSlotDTO);
    void updateTimeSlot(TimeSlotDTO timeSlotDTO);
    void deleteTimeSlot(int id);
    List<TimeSlotDTO> getAllTimeSlots();
    List<TimeSlotDTO> getTimeSlotsByPlaceID(int placeID);
    List<TimeSlotDTO> getTimeSlotsByEmployeeID(int employeeID);
    List<TimeSlotDTO> getTimeSlotsByPromotionID(int promotionID);
    List<TimeSlotDTO> getTimeSlotsByStartDate(LocalDateTime startDate);
    TimeSlotDTO getLatestTimeSlot();
}
