package convert;

import dto.PlaceDTO;
import entities.PlaceEntity;

import java.util.List;

public class PlaceConvert {
    private static PlaceConvert instance;

    public static PlaceConvert getInstance() {
        if (instance == null) {
            instance = new PlaceConvert();
        }
        return instance;
    }

    public PlaceDTO convertEntityToDTO(final PlaceEntity placeEntity) {
        final PlaceDTO placeDTO = new PlaceDTO();
        if (placeEntity != null) {
            placeDTO.setPlaceIdDTO(placeEntity.getPlaceID());
            placeDTO.setRoomDTO(placeEntity.getRoom());
            placeDTO.setRoomTypeDTO(placeEntity.getRoomType());
            placeDTO.setBuildingDTO(placeEntity.getBuilding());
            placeDTO.setDeviceIDsDTO(placeEntity.getDeviceIDs());
        }
        return placeDTO;
    }

    public List<PlaceDTO> convertEntityToDTO(final List<PlaceEntity> placeEntities) {
        return placeEntities.stream().map(this::convertEntityToDTO).toList();
    }

    public PlaceEntity convertDTOToEntity(final PlaceDTO placeDTOs) {
        final PlaceEntity placeEntity = new PlaceEntity();
        if (placeDTOs != null) {
            placeEntity.setPlaceID(placeDTOs.getPlaceIdDTO());
            placeEntity.setRoom(placeDTOs.getRoomDTO());
            placeEntity.setRoomType(placeDTOs.getRoomTypeDTO());
            placeEntity.setBuilding(placeDTOs.getBuildingDTO());
            placeEntity.setDeviceIDs(placeDTOs.getDeviceIDsDTO());
        }
        return placeEntity;
    }

    public List<PlaceEntity> convertDTOToEntity(final List<PlaceDTO> placeDTOs){
        return placeDTOs.stream().map(this::convertDTOToEntity).toList();
    }
}
