package convert;

import dto.TimeSlotDTO;
import entities.TimeSlotEntity;

import java.util.List;

public class TimeSlotConvert {
    private static TimeSlotConvert instance;

    private TimeSlotConvert() {
    }

    public static TimeSlotConvert getInstance() {
        if (instance == null) {
            instance = new TimeSlotConvert();
        }
        return instance;
    }

    public TimeSlotDTO convertEntityToDTO(final TimeSlotEntity TimeSlotEntity) {
        final TimeSlotDTO timeSlotDTO = new TimeSlotDTO();
        if (TimeSlotEntity != null) {
            timeSlotDTO.setTimeslotIdDTO(TimeSlotEntity.getTimeslotID());
            timeSlotDTO.setPlaceIdDTO(TimeSlotEntity.getPlaceID());
            timeSlotDTO.setEmployeeIdsDTO(TimeSlotEntity.getEmployeeIDs());
            timeSlotDTO.setPromotionIdsDTO(TimeSlotEntity.getPromotionIDs());
            timeSlotDTO.setMaterialIdDTO(TimeSlotEntity.getMaterialID());
            timeSlotDTO.setStartDateDTO(TimeSlotEntity.getStartDate());
            timeSlotDTO.setEndDateDTO(TimeSlotEntity.getEndDate());
        }
        return timeSlotDTO;
    }

    public List<TimeSlotDTO> convertEntityToDTO(final List<TimeSlotEntity> TimeSlotEntities) {
        return TimeSlotEntities.stream().map(this::convertEntityToDTO).toList();
    }

    public TimeSlotEntity convertDTOToEntity(final TimeSlotDTO timeSlotDTO) {
        final TimeSlotEntity TimeSlotEntity = new TimeSlotEntity();
        if (timeSlotDTO != null) {
            TimeSlotEntity.setTimeslotID(timeSlotDTO.getTimeslotIdDTO());
            TimeSlotEntity.setPlaceID(timeSlotDTO.getPlaceIdDTO());
            TimeSlotEntity.setEmployeeIDs(timeSlotDTO.getEmployeeIdsDTO());
            TimeSlotEntity.setPromotionIDs(timeSlotDTO.getPromotionIdsDTO());
            TimeSlotEntity.setMaterialID(timeSlotDTO.getMaterialIdDTO());
            TimeSlotEntity.setStartDate(timeSlotDTO.getStartDateDTO());
            TimeSlotEntity.setEndDate(timeSlotDTO.getEndDateDTO());
        }
        return TimeSlotEntity;
    }

    public List<TimeSlotEntity> convertDTOToEntity(final List<TimeSlotDTO> timeSlotDTOs) {
        return timeSlotDTOs.stream().map(this::convertDTOToEntity).toList();
    }
}
