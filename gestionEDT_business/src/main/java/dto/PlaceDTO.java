package dto;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

public class PlaceDTO implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    private int placeIdDTO;
    private String roomDTO;
    private String roomTypeDTO;
    private String buildingDTO;
    private List<Integer> deviceIDsDTO;

    public int getPlaceIdDTO() {
        return placeIdDTO;
    }

    public void setPlaceIdDTO(int placeIdDTO) {
        this.placeIdDTO = placeIdDTO;
    }

    public String getRoomDTO() {
        return roomDTO;
    }

    public void setRoomDTO(String roomDTO) {
        this.roomDTO = roomDTO;
    }

    public String getRoomTypeDTO() {
        return roomTypeDTO;
    }

    public void setRoomTypeDTO(String roomTypeDTO) {
        this.roomTypeDTO = roomTypeDTO;
    }

    public String getBuildingDTO() {
        return buildingDTO;
    }

    public void setBuildingDTO(String buildingDTO) {
        this.buildingDTO = buildingDTO;
    }

    public List<Integer> getDeviceIDsDTO() {
        return deviceIDsDTO;
    }

    public void setDeviceIDsDTO(List<Integer> deviceIDsDTO) {
        this.deviceIDsDTO = deviceIDsDTO;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof PlaceDTO place)) {
            return false;
        }
        return placeIdDTO == place.placeIdDTO &&
                roomDTO.equals(place.roomDTO) &&
                roomTypeDTO.equals(place.roomTypeDTO) &&
                buildingDTO.equals(place.buildingDTO) &&
                deviceIDsDTO.equals(place.deviceIDsDTO);
    }
}
