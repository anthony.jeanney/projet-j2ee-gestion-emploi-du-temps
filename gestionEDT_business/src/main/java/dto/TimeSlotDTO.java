package dto;

import java.io.Serial;
import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;

public class TimeSlotDTO implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    private int timeslotIdDTO;
    private int placeIdDTO;
    private List<Integer> employeeIdsDTO;
    private List<Integer> promotionIdsDTO;
    private int materialIdDTO;
    private LocalDateTime startDateDTO;
    private LocalDateTime endDateDTO;

    public int getTimeslotIdDTO() {
        return timeslotIdDTO;
    }

    public void setTimeslotIdDTO(int timeslotIdDTO) {
        this.timeslotIdDTO = timeslotIdDTO;
    }

    public int getPlaceIdDTO() {
        return placeIdDTO;
    }

    public void setPlaceIdDTO(int placeIdDTO) {
        this.placeIdDTO = placeIdDTO;
    }

    public List<Integer> getEmployeeIdsDTO() {
        return employeeIdsDTO;
    }

    public void setEmployeeIdsDTO(List<Integer> employeeIdsDTO) {
        this.employeeIdsDTO = employeeIdsDTO;
    }

    public List<Integer> getPromotionIdsDTO() {
        return promotionIdsDTO;
    }

    public void setPromotionIdsDTO(List<Integer> promotionIdsDTO) {
        this.promotionIdsDTO = promotionIdsDTO;
    }

    public int getMaterialIdDTO() {
        return materialIdDTO;
    }

    public void setMaterialIdDTO(int materialIdDTO) {
        this.materialIdDTO = materialIdDTO;
    }

    public LocalDateTime getStartDateDTO() {
        return startDateDTO;
    }

    public void setStartDateDTO(LocalDateTime startDateDTO) {
        this.startDateDTO = startDateDTO;
    }

    public LocalDateTime getEndDateDTO() {
        return endDateDTO;
    }

    public void setEndDateDTO(LocalDateTime endDateDTO) {
        this.endDateDTO = endDateDTO;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof TimeSlotDTO timeSlot)) {
            return false;
        }
        return timeslotIdDTO == timeSlot.timeslotIdDTO &&
                placeIdDTO == timeSlot.placeIdDTO &&
                employeeIdsDTO.equals(timeSlot.employeeIdsDTO) &&
                promotionIdsDTO.equals(timeSlot.promotionIdsDTO) &&
                materialIdDTO == timeSlot.materialIdDTO &&
                startDateDTO.equals(timeSlot.startDateDTO) &&
                endDateDTO.equals(timeSlot.endDateDTO);
    }
}
