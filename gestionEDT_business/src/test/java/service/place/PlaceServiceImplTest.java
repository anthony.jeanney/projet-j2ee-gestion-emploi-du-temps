package service.place;

import convert.PlaceConvert;
import dto.PlaceDTO;
import entities.PlaceEntity;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repositories.IPlaceRepository;

import java.util.ArrayList;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PlaceServiceImplTest {

    @Mock
    private IPlaceRepository IPlaceRepository;

    @InjectMocks
    public PlaceServiceImpl placeService;

    @BeforeEach
    void setUp() {
    }

    @Test
    void test_init() {
        assert placeService.getAllPlaces().isEmpty();
    }

    @Test
    void test_add_select() {
        PlaceDTO obj = new PlaceDTO();

        obj.setPlaceIdDTO(0);
        obj.setBuildingDTO("test");
        obj.setDeviceIDsDTO(new ArrayList<>());
        obj.setRoomTypeDTO("test");
        obj.setRoomDTO("test");

        PlaceEntity place = PlaceConvert.getInstance().convertDTOToEntity(obj);

        when(IPlaceRepository.save(any(PlaceEntity.class))).thenReturn(place);
        placeService.addPlace(obj);

        when(IPlaceRepository.findById(obj.getPlaceIdDTO())).thenReturn(Optional.of(place));
        assert placeService.getPlaceByID(obj.getPlaceIdDTO()).equals(obj);
    }

    @AfterEach
    void tearDown() {
    }
}